﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analisador
{
    class Lexico
    {

        /**
         * Tokens da linguagem
         * @var enum
         */
        public enum Tokens
        {
            AbreBloco,
            FechaBloco,
            Soma,
            Subtracao,
            Multiplicacao,
            Divisao,
            Exponenciacao,
            DivisaoInteira,
            Modulo,
            Maior,
            Menor,
            Igual,
            MaiorIgual,
            MenorIgual,
            Diferente,
            And,
            Or,
            Not,
            Atribuicao,
            SomaAtribuicao,
            SubtracaoAtribuicao,
            DoisPontos,
            AbreParenteses,
            FechaParenteses,
            AbreColchetes,
            FechaColchetes,
            Virgula,
            Ponto,
            PontoVirgula,
            False,
            None,
            True,
            As,
            Assert,
            Break,
            Class,
            Continue,
            Def,
            Del,
            Elif,
            Else,
            Except,
            Exec,
            Finally,
            For,
            From,
            Global,
            If,
            Import,
            In,
            Is,
            Lambda,
            Nonlocal,
            Pass,
            Raise,
            Return,
            Try,
            While,
            With,
            Yield,
            Identificador,
            String,
            Inteiro,
            Float,
            MultiplicacaoAtribuicao,
            DivisaoAtribuicao,
            ModuloAtribuicao
        };


        /**
         * Palavras Reservadas
         * @var Dictionary
         */
        private Dictionary<string, Tokens> palavrasReservadas = new Dictionary<string, Tokens>()
        {
            {"and", Tokens.And},
            {"def", Tokens.Def},
            {"exec", Tokens.Exec},
            {"if", Tokens.If},
            {"not", Tokens.Not},
            {"return", Tokens.Return},
            {"assert", Tokens.Assert},
            {"del", Tokens.Del},
            {"finally", Tokens.Finally},
            {"import", Tokens.Import},
            {"or", Tokens.Or},
            {"try", Tokens.Try},
            {"break", Tokens.Break},
            {"elif", Tokens.Elif},
            {"for", Tokens.For},
            {"raise", Tokens.Raise},
            {"in", Tokens.In},
            {"pass", Tokens.Pass},
            {"while", Tokens.While},
            {"class", Tokens.Class},
            {"else", Tokens.Else},
            {"from", Tokens.From},
            {"is", Tokens.Is},
            {"yield", Tokens.Yield},
            {"continue", Tokens.Continue},
            {"except", Tokens.Except},
            {"global", Tokens.Global},
            {"lambda", Tokens.Lambda},
            {"False", Tokens.False},
            {"None", Tokens.None},
            {"True", Tokens.True},
            {"as", Tokens.As},
            {"with", Tokens.With},
            {"nonlocal", Tokens.Nonlocal}
        };


        /**
         * Quantidade de tabs da linha anterior
         * @var int
         */
        private int tabs_ant = 0;

        /**
         *
         * @var int
         */
        private int tabs_dif = 0;


        /**
         * String para análise
         * @var char[]
         */
        private char[] codigo;


        /**
         * Contador do proximo
         * @var int
         */
        private int position = 0;


        /**
         * Contador para definir linha
         * @var int
         */
        private int linha  = 0;


        /**
         * Contador para definir coluna
         * @var int
         */
        private int coluna = 0;


        /**
         * Estado do analisador
         * @var int
         */
        private int estado = 0;


        /**
         * Construtor da classe
         */
        public Lexico(String codigo)
        {
            this.codigo = ('\n' + codigo + '\n' + ' ').ToCharArray();
        }


        /**
         * Retorna o próximo token
         * @return Token
         */
        public Token proximo()
        {
            Token tk = new Token();

            estado = 0;

            while (position < codigo.Length)
            {

                char atual = codigo[position];
                coluna++;

                /**
                 * ESTADOS
                 * 0 => OPERADORES E ABRE/FECHA BLOCO
                 * 1 => PALAVRAS RESERVADAS E IDENTIFICADORES
                 * 2 => CONSTANTES (INTEIRO E FLOAT)
                 * 3 => STRING
                 * 4 => COMENTARIOS
                 */

                switch (estado)
                {
                    // RECONHECE OPERADORES
                    case 0:

                        tk.lexema = codigo[position++].ToString();
                        tk.linha = linha;
                        tk.coluna = coluna;

                        if (tabs_dif != 0) {

                            coluna--;

                            // GERA OS TOKENS DE BLOCO CONFORME CALCULADO ABAIXO
                            if (tabs_dif > 0)
                            {
                                tk.token = Tokens.AbreBloco;
                                // CASO FOR 4 ESPAÇOS
                                if (atual == ' ')
                                {
                                    tk.lexema = "    ";
                                    position += 3;
                                }
                                tabs_dif--;
                            }
                            else
                            {
                                tk.token = Tokens.FechaBloco;
                                tk.lexema = "";
                                position--;
                                tabs_dif++;
                            }

                            
                            return tk;
                        }
                        else if (atual == '\n')
                        {
                            // QUEBRA DE LINHA MODIFICA CONTADOR
                            linha++;

                            // CALCULA TABS DA NOVA LINHA
                            int i = 0;
                            int tabs = 0;
                            int espacos = 0;
                            while (position < (codigo.Length - 1) && (codigo[position + i] == '\t' || codigo[position + i] == ' '))
                            {
                                if (codigo[position + i] == '\t')
                                    tabs++;
                                else
                                    espacos++;

                                i++;
                            }

                            coluna = tabs + espacos;

                            tabs += (int) Math.Floor(espacos / 4.0);
                            tabs_dif = tabs - tabs_ant;
                            tabs_ant = tabs;
                        }
                        else if ((atual >= 'a' && atual <= 'z') || (atual >= 'A' && atual <= 'Z') || atual == '_')
                        {
                            // IDENTIFICA IDENTIFICADOR OU PALAVRA RESERVADA
                            tk.token = Tokens.Identificador;
                            estado = 1;
                        }
                        else if (atual >= '0' && atual <= '9')
                        {
                            // IDENTIFICA CONSTANTES
                            tk.token = Tokens.Inteiro;
                            estado = 2;
                        }
                        else if (atual == '"' || atual == '\'')
                        {
                            // IDENTIFICA STRING
                            tk.token = Tokens.String;
                            estado = 3;
                        }
                        else if (atual == '#')
                        {
                            estado = 4;
                        }
                        else if(atual != ' ' && atual != '\t')
                        {

                            switch (atual)
                            {

                                // OPERADORES ARITMETICOS
                                case '+':
                                    if (codigo[position] == '=')
                                    {
                                        tk.token = Tokens.SomaAtribuicao;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else
                                        tk.token = Tokens.Soma;
                                    break;

                                case '-':
                                    if (codigo[position] == '=')
                                    {
                                        tk.token = Tokens.SubtracaoAtribuicao;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else
                                        tk.token = Tokens.Subtracao;
                                    break;

                                case '*':
                                    if (codigo[position] == '*')
                                    {
                                        tk.token = Tokens.Exponenciacao;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else if(codigo[position] == '=')
                                    {
                                        tk.token = Tokens.MultiplicacaoAtribuicao;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else
                                    {
                                        tk.token = Tokens.Multiplicacao;
                                    }
                                    break;

                                case '/':
                                    if (codigo[position] == '/')
                                    {
                                        tk.token = Tokens.DivisaoInteira;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else if (codigo[position] == '=')
                                    {
                                        tk.token = Tokens.DivisaoAtribuicao;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else
                                    {
                                        tk.token = Tokens.Divisao;
                                    }
                                    break;

                                case '%':
                                    if (codigo[position] == '=')
                                    {
                                        tk.token = Tokens.ModuloAtribuicao;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else
                                    {
                                        tk.token = Tokens.Modulo;
                                    }
                                    break;

                                // OPERADORES RELACIONAIS

                                case '>':
                                    if (codigo[position] == '=')
                                    {
                                        tk.token = Tokens.MaiorIgual;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else
                                        tk.token = Tokens.Maior;
                                    break;

                                case '<':
                                    if (codigo[position] == '=')
                                    {
                                        tk.token = Tokens.MenorIgual;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else
                                        tk.token = Tokens.Menor;
                                    break;

                                case '=':
                                    if (codigo[position] == '=')
                                    {
                                        tk.token = Tokens.Igual;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else
                                        tk.token = Tokens.Atribuicao;
                                    break;

                                case '!':
                                    if (codigo[position] == '=')
                                    {
                                        tk.token = Tokens.Diferente;
                                        tk.lexema += codigo[position++].ToString();
                                        coluna++;
                                    }
                                    else
                                    {
                                        throw new Exception("ERROR: O caracter " + atual + " não foi reconhecido - L:" + linha + " C:" + coluna);
                                    }
                                    break;

                                // OUTROS SIMBOLOS

                                case '[':
                                    tk.token = Tokens.AbreColchetes;
                                    break;

                                case ']':
                                    tk.token = Tokens.FechaColchetes;
                                    break;

                                case '(':
                                    tk.token = Tokens.AbreParenteses;
                                    break;

                                case ')':
                                    tk.token = Tokens.FechaParenteses;
                                    break;

                                case ':':
                                    tk.token = Tokens.DoisPontos;
                                    break;

                                case ';':
                                    tk.token = Tokens.PontoVirgula;
                                    break;

                                case ',':
                                    tk.token = Tokens.Virgula;
                                    break;

                                case '.':
                                    if (codigo[position] >= '0' && codigo[position] <= '9')
                                    {
                                        tk.token = Tokens.Float;

                                        tk.lexema += codigo[position++].ToString();
                                        estado = 2;
                                        break;
                                    }
                                    tk.token = Tokens.Ponto;
                                    break;

                                default:
                                    throw new Exception("ERROR: O caracter "+atual+" não foi reconhecido - L:"+linha+" C:"+coluna);
                            }
                            return tk;
                        }

                        break; /** FINALIZA ESTADO 0 */


                    // RECONHECE PALAVRAS RESERVADAS E IDENTIFICADORES
                    case 1:
                        if ((atual >= 'a' && atual <= 'z') || (atual >= 'A' && atual <= 'Z') || (atual >= '0' && atual <= '9') || atual == '_')
                        {
                            tk.lexema += codigo[position++].ToString();

                            atual = codigo[position];

                            if ((atual < 'a' || atual > 'z') && (atual < 'A' || atual > 'Z') && (atual < '0' || atual > '9') && atual != '_')
                            {
                                foreach (KeyValuePair<string, Tokens> palavra in palavrasReservadas)
                                {
                                    if (palavra.Key == tk.lexema)
                                    {
                                        tk.token = palavra.Value;
                                        return tk;
                                    }
                                }
                            }

                        }
                        else
                        {
                            coluna--;
                            return tk;
                        }
                        break;

                    // RECONHECE CONSTANTES (INTEIRO E FLOAT)
                    case 2:
                        if ((atual >= '0' && atual <= '9') || (atual == '.' && tk.token == Tokens.Inteiro))
                        {
                            if (atual == '.')
                                tk.token = Tokens.Float;

                            tk.lexema += codigo[position++].ToString();
                        }
                        else
                        {
                            coluna--;
                            return tk;
                        }
                        break;
                    
                    // RECONHECE STRINGS
                    case 3:
                        tk.lexema += codigo[position++].ToString();
                        if (atual.ToString() == tk.lexema.Substring(0,1))
                        {
                            coluna--;
                            return tk;
                        }
                        break;
                    
                    // COMENTARIOS
                    case 4:
                        if (atual == '\n')
                        {
                            position--;
                            estado = 0;
                        }
                        position++;
                        break;

                } /** ESTADO SWICTH */

            } /** WHILE */

            // CHEGOU AO FIM DO ARQUIVO
            if (estado == 3)
            {
                throw new Exception("ERROR: Esperava encontrar " + tk.lexema.Substring(0, 1) + " - L:" + linha + " C:" + coluna);
            }
            throw new Exception("EOF");
        }

    }
}
