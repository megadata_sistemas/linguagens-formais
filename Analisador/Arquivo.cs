﻿using System;
using System.IO;
using System.Text;

namespace Analisador
{
    class Arquivo
    {
        /**
         * FileStream criado no constructor
         */
        private FileStream stream;
        private StreamWriter writer;


        /**
         * Contrutor da classe
         */
        public Arquivo(String path)
        {
            stream = File.OpenWrite(path);
            writer = new StreamWriter(stream, Encoding.Default);
            stream.SetLength(0);
        }


        /**
         * Transforma o token em string e escreve
         */
        public void escreveToken(Token token)
        {
            String content = "TOKEN: " + token.token.ToString().PadRight(15) + " - LINHA: " + token.linha.ToString().PadLeft(5) + " - COLUNA: " + token.coluna.ToString().PadLeft(5) + " - LEXEMA: " + token.lexema + "\n";
            writer.Write(content);
            //writer.Flush();
        }

        public void escreveC3E(String p)
        {
            writer.Write(p);
        }

        /**
         * Fecha o FileStream
         */
        public void fecha()
        {
            writer.Close();
            stream.Close();
        }
    }
}
