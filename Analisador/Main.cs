﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analisador
{
    public partial class Main : Form
    {

        /**
         * Path do arquivo .lex
         */
        private String pathLexico = "c:\\temp\\lexico.lex";
        public static String pathC3E = "c:\\temp\\C3E.c3e";

        private List<Token> ListaToken = new List<Token>();


        /**
         * Construtor da classe
         */
        public Main()
        {
            InitializeComponent();
            //Seta o tamanho do tab para ficar como se fosse 4 espacos
            TxtCodigo.SelectionTabs = new int[] { 10, 25, 35, 50, 65, 75, 85, 100, 110, 125, 135, 150, 165, 175, 185 };
            button1.Enabled = false;
        }


        /**
         * Gera o arquivo de texto lexixo.lex
         */
        private void lexico_Click(object sender, EventArgs e)
        {

            String codigo = TxtCodigo.Text.ToString();

            Lexico lexico = new Lexico(codigo);
            Arquivo arq = new Arquivo(this.pathLexico);

            ListaToken = new List<Token>();

            txtOutput.Text = "ANALISE LEXICA INICIADA\n-------------------------------------------------------------------------";
            try
            {
                while (true)
                {
                    Token token = lexico.proximo();
                    txtOutput.Text += "\nTK: " + token.token.ToString().PadRight(15) + " - L: "+ token.linha.ToString().PadLeft(5) +" - C: " + token.coluna.ToString().PadLeft(5) + " - LEX: " + token.lexema;
                    arq.escreveToken(token);
                    ListaToken.Add(token);
                }
            }catch(Exception ex){
                if (ex.Message == "EOF")
                {
                    txtOutput.Text += "\n-------------------------------------------------------------------------\nANALISE LEXICA FINALIZADA";
                    
                }
                else
                {
                    txtOutput.Text += "\n-------------------------------------------------------------------------\nERRO DE EXECUÇÃO: " + ex.Message;
                }
            }
            button1.Enabled = true;
            arq.fecha();
        }

        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            if (button1.Enabled == true) { 
                button1.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (ListaToken.Count > 0)
            {
                try
                {
                    Sintatico sintatico = new Sintatico(ListaToken);
                    sintatico.IniciaReconhecimento();

                }
                catch (Exception ex)
                {
                    if (ex.Message == "EOF")
                    {
                        txtOutput.Text += "\n-------------------------------------------------------------------------\nANALISE SINTATICA FINALIZADA";
                    }
                    else
                    {
                        txtOutput.Text += "\n-------------------------------------------------------------------------\nERRO DE EXECUÇÃO: " + ex.Message;
                    }
                }
            }
            else
            {
                txtOutput.Text += "\n-------------------------------------------------------------------------\nCODIGO VAZIO";
            }

        }
    }
}
