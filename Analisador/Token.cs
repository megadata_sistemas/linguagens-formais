﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analisador
{
    class Token
    {
        /**
         * Linha em que foi encontrado
         * @var int
         */
        public int linha;

        /**
         * Coluna em que foi encontrado
         * @var int
         */
        public int coluna;

        /**
         * Id do Token
         * @var int
         */
        public Lexico.Tokens token;

        /**
         * Token reconhecido
         * @var string
         */
        public String lexema;
        
    }
}
