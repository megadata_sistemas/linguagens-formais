﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analisador
{
    class Sintatico
    {
        private List<Token> listaToken;
        private int indiceAtual = 0;
        private Token tokenAtual;
        private int blocoAberto = 0;
        private Boolean fimArquivo = false;
        private List<Variavel> tabela = new List<Variavel>();
        private int qtdeTemp;
        private int qtdeLabel;

        public Sintatico(List<Token> listaToken)
        {
            this.listaToken = listaToken;
        }

        private void proximoToken()
        {
            if(indiceAtual < listaToken.Count)
            {
                tokenAtual = listaToken[indiceAtual];
                indiceAtual++;
                Console.WriteLine("token lido "+ indiceAtual + "de"+ listaToken.Count + "TK: " + tokenAtual.lexema);
            }
            else
            {
                fimArquivo = true;
            }
        }

        private void voltaToken()
        {
            indiceAtual--;
            tokenAtual = listaToken[indiceAtual];
        }

        public void IniciaReconhecimento()
        {
            Producao com = new Producao();
            ListaComandos(ref com);
            Arquivo arq = new Arquivo(Main.pathC3E);
            arq.escreveC3E(com.cod);
            arq.fecha();
            throw new Exception("EOF");
        }

        private void ListaComandos(ref Producao lCom)
        {
            proximoToken();
            if (!fimArquivo)
            {
                //ok
                if (tokenAtual.token == Lexico.Tokens.While)
                {
                    Producao com = new Producao();
                    gWhile(ref com);
                    lCom.cod = lCom.cod + com.cod;
                    Console.WriteLine(lCom.cod);
                    ListaComandos(ref lCom);
                }
                else if (tokenAtual.token == Lexico.Tokens.For)
                {
                    Producao com = new Producao();
                    gFor(ref com);
                    lCom.cod = lCom.cod + com.cod;
                    Console.WriteLine(lCom.cod);
                    ListaComandos(ref lCom);
                }
                //ok
                else if (tokenAtual.token == Lexico.Tokens.If)
                {
                    Producao com = new Producao();
                    gIf(ref com);
                    lCom.cod = lCom.cod + com.cod;
                    Console.WriteLine(lCom.cod);
                    ListaComandos(ref lCom);
                }
                //falta chamada de função
                else if (tokenAtual.token == Lexico.Tokens.Identificador)
                {
                    proximoToken();
                    if(tokenAtual.token == Lexico.Tokens.AbreParenteses)
                    {
                        proximoToken();
                        chamadaFuncao();
                    }
                    else
                    {

                        voltaToken();
                        voltaToken();
                        String lexema = tokenAtual.lexema;
                        Producao atrib = new Producao();
                        Variavel.Tipos tipo = Variavel.Tipos.Null;
                        OP4(ref tipo, ref atrib);
                        proximoToken();
                        proximoToken();
                        operadorAtrib(ref tipo, ref atrib);
                        lCom.cod = lCom.cod + atrib.cod ;
                        Console.WriteLine(lCom.cod);
                        inserirTabela(tipo, lexema);
                    }
                    ListaComandos(ref lCom);
                }
                //ok
                else if (tokenAtual.token == Lexico.Tokens.Return)
                {
                    lCom.cod += "goto " + lCom.labelFim + "\n";
                    return;
                }
                else if (tokenAtual.token == Lexico.Tokens.Def)
                {
                    gDefFuncao();
                    ListaComandos(ref lCom);
                }
                else
                {
                    voltaToken();
                    return;
                }
            }
            else
            {
                return;
            }

        }

        /*************************************
         * Gramatica para Definicao de Funcao
         *************************************/
        private void gDefFuncao()
        {

            //identificador();descomentar

            proximoToken();
            if (tokenAtual.token != Lexico.Tokens.AbreParenteses)
            {
                throw new Exception("ERROR: Esperava um '(' na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }
            argumentos();

            proximoToken();

            if (tokenAtual.token != Lexico.Tokens.FechaParenteses)
            {
                throw new Exception("ERROR: Esperava um ')' na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }

            gDoisPontos();

            abreBloco();

            //ListaComandos();descomentar

            fechaBloco();
        }

        /*************************************
         * Gramatica para Chamada de Funcao
         *************************************/
        private void chamadaFuncao()
        {
            if(tokenAtual.token != Lexico.Tokens.FechaParenteses)
            {
               // rParametros();

               // rValorAtribuicao();descomentarso

                proximoToken();

                if(tokenAtual.token != Lexico.Tokens.FechaParenteses)
                {
                    throw new Exception("ERROR: Esperava um ')' na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
                }
            }
        }

        /*************************************************
         * Gramatica para parametros da definicao de funcao
         ************************************************/
        private void argumentos()
        {
            proximoToken();

            if(tokenAtual.token == Lexico.Tokens.Identificador)
            {
                rAgumentos();
            }
            else
            {
                voltaToken();
            }
        }

        /***************************************************************
         * Gramatica para o resto dos parametros da definicao de funcao
         ***************************************************************/
        private void rAgumentos()
        {
            proximoToken();

            if (tokenAtual.token == Lexico.Tokens.Virgula)
            {
                //identificador();descomentar

                rAgumentos();
            }
            else
            {
                voltaToken();
            }
        }


        /*************************************
         * Gramatica para Atribuicoes
         *************************************/
        //validado para ser somente uma atribuicao por vez.
        private void gAtribuicao(ref Variavel.Tipos tipo, ref Producao atrib)
        {
            operadorAtrib(ref tipo, ref atrib);
        }

        /*************************************
         * Gramatica para operador da atribuicao
         *************************************/
        private void operadorAtrib(ref Variavel.Tipos tipo, ref Producao atrib)
        {
            if (tokenAtual.token != Lexico.Tokens.Atribuicao &&
                tokenAtual.token != Lexico.Tokens.SomaAtribuicao &&
                tokenAtual.token != Lexico.Tokens.SubtracaoAtribuicao &&
                tokenAtual.token != Lexico.Tokens.MultiplicacaoAtribuicao &&
                tokenAtual.token != Lexico.Tokens.DivisaoAtribuicao &&
                tokenAtual.token != Lexico.Tokens.ModuloAtribuicao)
            {
                throw new Exception("ERROR: Esperava um operador '=' | '+=' | '-=' | '*=' | '/=' | '%=' na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }
            Producao op = new Producao();
            Producao atrib2 = new Producao();
            if (tokenAtual.token == Lexico.Tokens.Atribuicao)
            {
                proximoToken();
                OP1(ref tipo, ref atrib2);
                Console.WriteLine(atrib2.cod);
                atrib.cod = atrib.cod + atrib2.cod + atrib.place + " := " + atrib2.place + "\n";
            }
            if (tokenAtual.token == Lexico.Tokens.SomaAtribuicao)
            {
                proximoToken();
                OP1(ref tipo, ref atrib2);
                Console.WriteLine(atrib2.cod);
                Console.WriteLine(atrib2.cod);
                atrib.cod = atrib.cod + atrib2.cod + atrib.place + " := " + atrib.place + " + " + atrib2.place + "\n";
            }
            if (tokenAtual.token == Lexico.Tokens.SubtracaoAtribuicao)
            {
                proximoToken();
                OP1(ref tipo, ref atrib2);
                Console.WriteLine(atrib2.cod);
                atrib.cod = atrib.cod + atrib2.cod + atrib.place + " := " + atrib.place + " - " + atrib2.place + "\n";
            }
            if (tokenAtual.token == Lexico.Tokens.MultiplicacaoAtribuicao)
            {
                proximoToken();
                OP1(ref tipo, ref atrib2);
                Console.WriteLine(atrib2.cod);
                atrib.cod = atrib.cod + atrib2.cod + atrib.place + " := " + atrib.place + " * " + atrib2.place + "\n";
            }
            if (tokenAtual.token == Lexico.Tokens.DivisaoAtribuicao)
            {
                proximoToken();
                OP1(ref tipo, ref atrib2);
                Console.WriteLine(atrib2.cod);
                atrib.cod = atrib.cod + atrib2.cod + atrib.place + " := " + atrib.place + " / " + atrib2.place + "\n";
            }
            if (tokenAtual.token == Lexico.Tokens.ModuloAtribuicao)
            {
                proximoToken();
                OP1(ref tipo, ref atrib2);
                Console.WriteLine(atrib2.cod);
                atrib.cod = atrib.cod + atrib2.cod + atrib.place + " := " + atrib.place + " // " + atrib2.place + "\n";
            }
            
        }

        /*************************************
         * Gramatica para o valor da atribuicao
         *************************************/

        private void valorAtribuicao(ref Variavel.Tipos tipo)
        {
            proximoToken();
        }

        /*************************************
         * Gramatica para os operandos
         *************************************/
        /*private void operando()
        {
            if(tokenAtual.token != Lexico.Tokens.Atribuicao && 
                tokenAtual.token != Lexico.Tokens.SomaAtribuicao &&
                tokenAtual.token != Lexico.Tokens.SubtracaoAtribuicao &&
                tokenAtual.token != Lexico.Tokens.MultiplicacaoAtribuicao &&
                tokenAtual.token != Lexico.Tokens.DivisaoAtribuicao &&
                tokenAtual.token != Lexico.Tokens.ModuloAtribuicao)
            {
                throw new Exception("ERROR: Esperava um operador '=' | '+=' | '-=' | '*=' | '/=' | '%=' na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }
        }*/

        /*************************************
         * Gramatica para IF
         *************************************/
        private void gIf(ref Producao com)
        {
            Producao pteste = new Producao();
            pteste.labelFalse = gera_label();
            pteste.labelFim = gera_label();
            pteste.labelTrue = gera_label();
            gTeste(ref pteste);

            gDoisPontos();

            abreBloco();
            Producao lcom = new Producao();
            lcom.labelFim = pteste.labelFim;
            ListaComandos(ref lcom);

            Console.WriteLine("voltou do comando");
            fechaBloco();

            Producao pElse = new Producao();
            pElse.labelFim = pteste.labelFim;
            Console.WriteLine("entrou no else com o token " + tokenAtual.lexema);
            gElse(ref pElse);

            com.cod = com.cod + pteste.cod + "if " + pteste.place + " = 0 goto " + pteste.labelTrue + " \n" + "goto " + pteste.labelFalse + "\n" 
                    + pteste.labelTrue + ": " + lcom.cod + "goto " + pteste.labelFim + "\n" + pteste.labelFalse + ":\n" + pElse.cod + pteste.labelFim + ":\n";


        }

        /*************************************
         * Gramatica para ELSE
         *************************************/
        private void gElse(ref Producao p)
        {
            proximoToken();

            if(tokenAtual.token == Lexico.Tokens.Else)
            {
                gDoisPontos();

                abreBloco();

                Producao pCom = new Producao();
                pCom.labelFim = p.labelFim;
                ListaComandos(ref pCom);

                p.cod = pCom.cod;
                p.place = pCom.place;

                fechaBloco();
            }
            else if( tokenAtual.token == Lexico.Tokens.Elif)
            {
                Producao pTeste = new Producao();
                gTeste(ref pTeste);
                pTeste.labelFalse = gera_label();
                pTeste.labelFim = gera_label();

                gDoisPontos();

                abreBloco();

                Producao pCom = new Producao();
                pCom.labelFim = pTeste.labelFim;
                ListaComandos(ref pCom);

                fechaBloco();

                Producao pElse2 = new Producao();
                pElse2.labelFim = pTeste.labelFim;
                gElse(ref pElse2);

                p.cod = p.cod  + pTeste.cod + " if " + pTeste.place + " <> 0 goto " + pTeste.labelFalse + " \n"
                    + pCom.cod + " goto " + pTeste.labelFim + "\n" + pTeste.labelFalse + ": " + pElse2.cod  + pTeste.labelFim + ": \n";
            }
        }

        /*************************************
         * Gramatica para For
         *************************************/
        private void gFor(ref Producao p)
        {
            p.labelLaco = gera_label();
            p.labelFalse = gera_label();
            p.labelTrue = gera_label();
            proximoToken();
            Producao c1 = new Producao();
            Producao c2 = new Producao();
            Producao id = new Producao();

            Variavel.Tipos tipo = Variavel.Tipos.Int;
            identificador( ref id);
            inserirTabela(tipo, tokenAtual.lexema);
            proximoToken();
            if( tokenAtual.token == Lexico.Tokens.In)
            {
                proximoToken();
                if (tokenAtual.token == Lexico.Tokens.Identificador)
                {
                    proximoToken();

                    if (tokenAtual.token == Lexico.Tokens.AbreParenteses)
                    {
                        constante(ref c1);
                        proximoToken();
                        if (tokenAtual.token != Lexico.Tokens.Virgula)
                        {
                            throw new Exception("ERROR: Esperava uma virgula" + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
                        }
                        constante(ref c2);
                        proximoToken();
                        if (tokenAtual.token != Lexico.Tokens.FechaParenteses)
                        {
                            throw new Exception("ERROR: Esperava fecha parenteses" + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
                        }
                    }
                }
                else
                {
                    throw new Exception("ERROR: Esperava uma variavel, ou chamada de funcao na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
                }

                gDoisPontos();

                abreBloco();

                Producao pCom = new Producao();
                ListaComandos(ref pCom);

                fechaBloco();

                Producao pElse = new Producao();
                pElse.labelFim = p.labelFim;
                gElse(ref pElse);

                p.place = gera_temp();
                p.cod = p.cod + c1.cod + c2.cod + id.place + " = " + c1.place + "\n" + p.labelLaco + ": " + p.place + " = " + id.place + " < " + c2.place + "\n"
                      + "if " + p.place + " = 0 goto " + p.labelTrue + "\n" + "goto " + p.labelFalse + "\n" + p.labelTrue + ": "
                      + pCom.cod + id.place + " = " + id.place + " + 1 \ngoto " + p.labelLaco + "\n" + p.labelFalse + ":\n" + pElse.cod;
            }
        }

        /*************************************
         * Gramatica para Identificador
         *************************************/
        private void identificador(ref Producao E)
        {
            if( tokenAtual.token != Lexico.Tokens.Identificador)
            {
                throw new Exception("ERROR: Esperava um Identificador na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }
            else
            {
                E.place = tokenAtual.lexema;
                E.cod = "";
            }
        }

        /*************************************
         * Gramatica para While
         *************************************/
        private void gWhile(ref Producao com)
        {
            Producao pteste = new Producao();
            pteste.labelFim = gera_label();
            pteste.labelLaco = gera_label();
            gTeste(ref pteste);

            gDoisPontos();
            
            abreBloco();

            Producao pCom = new Producao();
            ListaComandos(ref pCom);

            fechaBloco();

            com.cod = pteste.labelLaco + ":\n" + pteste.cod + "if " + pteste.place + " <> 0 goto " + pteste.labelFim + " \n"
                    + pCom.cod + "goto " + pteste.labelLaco + "\n" + pteste.labelFim + ":\n";
        }

        /*************************************
         * Gramatica para Dois Pontos :
         *************************************/
        private void gDoisPontos()
        {
            proximoToken();

            if (tokenAtual.token != Lexico.Tokens.DoisPontos)
            {                
                throw new Exception("ERROR: Esperava o caracter ':' na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }
        }

        /*************************************
         * Gramatica para Iniciar os Testes
         *************************************/
        private void gTeste(ref Producao E)
        {
            proximoToken();

            gOr(ref E);

        }

        /*************************************
         * Gramatica para Or
         *************************************/
        private void gOr(ref Producao E)
        {
            gAnd(ref E);

            proximoToken();

            if(tokenAtual.token == Lexico.Tokens.Or)
            {
                proximoToken();
                Producao Or2 = new Producao();
                gOr(ref Or2);
                E.cod = E.cod + "if " + E.place + " = 0 goto " + E.labelTrue + "\n"
                        + Or2.cod;
                E.place = Or2.place;
            }
            else
            {
                voltaToken();
            }
        }

        /*************************************
         * Gramatica para And
         *************************************/
        private void gAnd(ref Producao E)
        {
            gNot(ref E);

            proximoToken();

            if (tokenAtual.token == Lexico.Tokens.And)
            {
                proximoToken();
                Producao And2 = new Producao();
                gAnd(ref And2);
                E.cod = E.cod + "if " + E.place + " <> 0 goto " + E.labelFalse + "\n"
                        + And2.cod;
                E.place = And2.place;
            }
            else
            {
                voltaToken();
            }
        }

        /*************************************
         * Gramatica para Not
         *************************************/
        private void gNot(ref Producao E)
        {
            if (tokenAtual.token != Lexico.Tokens.Not)
            {
                comparacao(ref E);
            }
            else
            {
                proximoToken();
                comparacao(ref E);
                E.cod = E.cod + E.place + ":= not " + E.place + "\n";
            }
        }

        /*************************************
         * Gramatica para Comparacao
         *************************************/
        private void comparacao(ref Producao E)
        {
            Producao E1 = new Producao();
            Variavel.Tipos tipo = Variavel.Tipos.Null;
            OP1(ref tipo, ref E);
            CMP(ref tipo, ref E);
        }

        /*************************************
         * Gramatica os comparadores
         *************************************/
        private void CMP(ref Variavel.Tipos tipo, ref Producao E1)
        {
            proximoToken();

            if(tokenAtual.token == Lexico.Tokens.Menor || 
                tokenAtual.token == Lexico.Tokens.Maior || 
                tokenAtual.token == Lexico.Tokens.MenorIgual || 
                tokenAtual.token == Lexico.Tokens.MaiorIgual || 
                tokenAtual.token == Lexico.Tokens.Diferente || 
                tokenAtual.token == Lexico.Tokens.Igual)
            {
                String op = "";
                if (tokenAtual.token == Lexico.Tokens.Menor) op = " < ";
                if (tokenAtual.token == Lexico.Tokens.Maior) op = " > ";
                if (tokenAtual.token == Lexico.Tokens.MenorIgual) op = " <= ";
                if (tokenAtual.token == Lexico.Tokens.MaiorIgual) op = " >= ";
                if (tokenAtual.token == Lexico.Tokens.Diferente) op = " <> ";
                if (tokenAtual.token == Lexico.Tokens.Igual) op = " = ";
                proximoToken();
                Producao E2 = new Producao();
                Variavel.Tipos tipo2 = Variavel.Tipos.Null;
                OP1(ref tipo2, ref E2);
                if(tipo != tipo2)
                {
                    throw new Exception("ERROR: Não pode ser comparado " + tipo + " com " + tipo2);
                }
                Producao E3 = new Producao();
                E3.place = gera_temp();
                E1.cod = E1.cod + E2.cod + E3.place + " := " + E1.place + op + E2.place + "\n";
                E1.place = E3.place;
            }
            else
            {
                voltaToken();
            }
        }

        /*************************************
         * Gramatica para soma e subracao
         *************************************/
        private void OP1(ref Variavel.Tipos tipo, ref Producao op1)
        {
            Producao op2 = new Producao();
            OP2(ref tipo, ref op2);

            proximoToken();

            if(tokenAtual.token == Lexico.Tokens.Soma || tokenAtual.token == Lexico.Tokens.Subtracao)
            {
                String op = "";
                if (tokenAtual.token == Lexico.Tokens.Soma) op = " + ";
                if (tokenAtual.token == Lexico.Tokens.Subtracao) op = " - ";
                proximoToken();
                Producao op11 = new Producao();
                Variavel.Tipos tipo2 = Variavel.Tipos.Null;
                OP1(ref tipo2, ref op11);
                op1.place = gera_temp();
                op1.cod = op2.cod + op11.cod + op1.place + " := " + op2.place + op + op11.place + "\n";
                Console.WriteLine(op1.cod);
            }
            else
            {
                op1.cod = op2.cod;
                op1.place = op2.place;
                voltaToken();
            }

        }

        /******************************************************************
         * Gramatica para multiplicacao, divisao, modulo e divisao inteira
         *****************************************************************/
        private void OP2(ref Variavel.Tipos tipo, ref Producao op2)
        {
            Producao op3 = new Producao();
            OP3(ref tipo, ref op3);

            proximoToken();

            if (tokenAtual.token == Lexico.Tokens.Multiplicacao || 
                tokenAtual.token == Lexico.Tokens.Divisao || 
                tokenAtual.token == Lexico.Tokens.Modulo || 
                tokenAtual.token == Lexico.Tokens.DivisaoInteira)
            {
                String op = "";
                if (tokenAtual.token == Lexico.Tokens.Multiplicacao) op = " * ";
                if (tokenAtual.token == Lexico.Tokens.Divisao) op = " / ";
                if (tokenAtual.token == Lexico.Tokens.Modulo) op = " | ";
                if (tokenAtual.token == Lexico.Tokens.DivisaoInteira) op = " \\ ";
                proximoToken();
                Variavel.Tipos tipo2 = Variavel.Tipos.Null;
                Producao op22 = new Producao();
                OP2(ref tipo2, ref op22);
                op2.place = gera_temp();
                op2.cod = op3.cod + op22.cod + op2.place + " := " + op3.cod + op + op22.cod + "\n";
                Console.WriteLine(op2.cod);
            }
            else
            {
                op2.cod = op3.cod;
                op2.place = op3.place;
                voltaToken();
            }
        }

        /*************************************
         * Gramatica para Exponenciacao
         *************************************/
        private void OP3(ref Variavel.Tipos tipo,ref Producao op3)
        {
            Producao op4 = new Producao();
            OP4(ref tipo, ref op4);

            proximoToken();

            if (tokenAtual.token == Lexico.Tokens.Exponenciacao)
            {
                proximoToken();
                Variavel.Tipos tipo2 = Variavel.Tipos.Null;
                //OP3(ref tipo2); 
            }
            else
            {
                voltaToken();
                op3.cod = op4.cod;
                op3.place = op4.place;
            }
        }

        /*************************************
         * Fim da Gramatica de Teste
         *************************************/
        private void OP4(ref Variavel.Tipos tipo, ref Producao E)
        {
            if (tokenAtual.token != Lexico.Tokens.Identificador &&
                tokenAtual.token != Lexico.Tokens.String &&
                tokenAtual.token != Lexico.Tokens.Inteiro &&
                tokenAtual.token != Lexico.Tokens.Float)
            {
                if(tokenAtual.token == Lexico.Tokens.AbreParenteses)
                {
                    Producao teste = new Producao();
                    gTeste(ref teste);

                    E.cod = teste.cod;
                    E.place = teste.place;

                    proximoToken();

                    if (tokenAtual.token != Lexico.Tokens.FechaParenteses)
                    {
                        throw new Exception("ERROR: Esperava o caracter ')' na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
                    }
                }
                else
                {
                    throw new Exception("ERROR: caracter inválido na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
                }
            }
            retornaTipo(ref tipo);
            if (tokenAtual.token == Lexico.Tokens.Identificador)
            {
                E.place = tokenAtual.lexema;
                E.cod = "";
            }
            if (tokenAtual.token == Lexico.Tokens.String || tokenAtual.token == Lexico.Tokens.Inteiro || tokenAtual.token == Lexico.Tokens.Float)
            {
                E.place = gera_temp();
                E.cod = E.place + " := " + tokenAtual.lexema + "\n";
            }
        }

       
        private bool validaVariavel(String lexema)
        {
            foreach( Variavel i in tabela)
            {
                if (i.lexema == lexema)
                    return true;
            }  
            return false;
        }

        /*************************************
         * Gramatica para abrir Identacao
         *************************************/
        private void abreBloco()
        {
            proximoToken();
            if(tokenAtual.token != Lexico.Tokens.AbreBloco)
            {
                throw new Exception("ERROR: Esperava abrir o bloco de identacao na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }
            blocoAberto++;
        }

        /*************************************
         * Gramatica para fechar Identacao
         *************************************/
        private void fechaBloco()
        {
            Console.WriteLine("FECHA BLOCO");

            proximoToken();
            if (tokenAtual.token != Lexico.Tokens.FechaBloco)
            {
                throw new Exception("ERROR: Esperava fechar o bloco de identacao na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }
            blocoAberto--;
        }
        
        /********************************************************
         * Gramatica para Constantes, String, Inteiro e Float
         *******************************************************/
        private void constante(ref Producao E)
        {
            proximoToken();

            if(tokenAtual.token != Lexico.Tokens.String &&
                tokenAtual.token != Lexico.Tokens.Inteiro && 
                tokenAtual.token != Lexico.Tokens.Float)
            {
                throw new Exception("ERROR: Esperava alguma constante(inteira, float ou String) na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }
            else
            {
                E.place = gera_temp();
                E.cod = E.place + " := " + tokenAtual.lexema + "\n";
            }
        }

        /****************************************************************************
         * Gramatica para resto do parametros identificador, String, Inteiro e Float
         ****************************************************************************/
        private void rParametros(ref Variavel.Tipos tipo)
        {
            //proximoToken();

            if (tokenAtual.token != Lexico.Tokens.Identificador &&
                tokenAtual.token != Lexico.Tokens.String &&
                tokenAtual.token != Lexico.Tokens.Inteiro &&
                tokenAtual.token != Lexico.Tokens.Float)
            {
                throw new Exception("ERROR: Esperava alguma constante(inteira, float ou String) ou um Identificador na Linha " + tokenAtual.linha + " Coluna " + tokenAtual.coluna);
            }
            else
            {
                retornaTipo(ref tipo);
            }
        }

        /****************************************************************************
         *                      insere a variavel na tabela
         ****************************************************************************/
        private void inserirTabela(Variavel.Tipos tipo, string lexema)
        {
            foreach (Variavel i in tabela)
            {
                if (i.lexema == lexema)
                {
                    i.tipo = tipo;
                    return;
                }
                    
            } 
            Variavel variavel = new Variavel();
            variavel.lexema = lexema;
            variavel.tipo = tipo;
            tabela.Add(variavel);
        }

        /****************************************************************************
         *      Retorna o tipo do tokenAtual
         ****************************************************************************/
        private void retornaTipo(ref Variavel.Tipos tipo)
        {
            switch (tokenAtual.token)
            {
                case Lexico.Tokens.String:
                    tipo = Variavel.Tipos.String;
                    break;
                case Lexico.Tokens.Inteiro:
                    tipo = Variavel.Tipos.Int;
                    break;
                case Lexico.Tokens.Float:
                    tipo = Variavel.Tipos.Float;
                    break;
                case Lexico.Tokens.Identificador:
                    String lexema = tokenAtual.lexema;
                    foreach (Variavel i in tabela)
                    {
                        if (i.lexema == tokenAtual.lexema)
                        {
                            tipo = i.tipo;
                            return;
                        }
                    }
                    proximoToken();
                    proximoToken();
                    if (tokenAtual.token == Lexico.Tokens.Atribuicao)
                    {
                        voltaToken();
                        voltaToken();
                        return;
                    }
                    throw new Exception("ERROR: variavel '" + lexema + "' não declarado!");
            }
        }

        /****************************************************************************
         *      Cria váriavel temporária
         ****************************************************************************/
        private String gera_temp()
        {
            String temp = "T" + String.Format(qtdeTemp.ToString(), "0#");
            qtdeTemp++;
            return temp;
        }

        /****************************************************************************
         *  cria os lebols
         ****************************************************************************/
        private String gera_label()
        {
            string temp = "LB" + qtdeLabel.ToString().PadLeft(2, '0');
            qtdeLabel++;
            return temp;
        }

    }
}

